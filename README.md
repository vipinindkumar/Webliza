# Webliza
Implementation of ELIZA(https://en.wikipedia.org/wiki/ELIZA) in python, with interface to chat with the bot.

Created as a final project for CS50 course.

![Screeshot of Webliza](/eliza%20screen.png)
